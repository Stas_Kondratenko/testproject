﻿using HelixToolkit.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TestProject.Tasks3D.ViewModel;

namespace TestProject.Tasks3D.Page {
    public partial class TaskOne : UserControl {

        public TaskOne() {
            InitializeComponent();
            DataContextChanged += TaskOneDataContextChanged;
            this.DataContext = new TaskOneViewModel();
            this.Loaded += TaskOneLoaded;
        }

        private void TaskOneLoaded(object sender, RoutedEventArgs e) {
            if (sender != null) {
                var view = (TaskOne)sender;
                var viewModel = (TaskOneViewModel)view.DataContext;
                viewModel.Draw();
            }
        }

        private void TaskOneDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if (sender != null) {
                var view = (TaskOne) sender;
                var viewModel = (TaskOneViewModel)view.DataContext;
                viewModel.Viewport = HViewPort;
            }     
        }
    }
}
