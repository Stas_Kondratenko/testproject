﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TestProject.Tasks3D.ViewModel;

namespace TestProject.Tasks3D.Page {
    /// <summary>
    /// Interaction logic for TaskTwo.xaml
    /// </summary>
    public partial class TaskTwo : UserControl {

        public TaskTwo() {
            InitializeComponent();
            DataContextChanged += TaskTwoDataContextChanged;
            this.DataContext = new TaskTwoViewModel();
            this.Loaded += TaskTwoLoaded;
            panel.Children.OfType<ModernToggleButton>().First().IsChecked = true;
        }

        private void TaskTwoDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if (sender != null) {
                var view = (TaskTwo)sender;
                var viewModel = (TaskTwoViewModel)view.DataContext;
                viewModel.Viewport = HViewPort;
            }
        }

        private void TaskTwoLoaded(object sender, RoutedEventArgs e) {
            if (sender != null) {
                var view = (TaskTwo)sender;
                var viewModel = (TaskTwoViewModel)view.DataContext;
                viewModel.Initialization();
            }
        }

        private void PanelButtonClicked(object sender, EventArgs e) {
           var buttons = panel.Children.OfType<ModernToggleButton>();
            foreach(var button in buttons) {
                if(button == (ModernToggleButton)sender) { button.IsChecked = true; continue; }
                button.IsChecked = false;
            }

        }
    }
}
