﻿using System.Windows;
using System.Windows.Controls;
using TestProject.Tasks3D.ViewModel;

namespace TestProject.Tasks3D.Page {
    public partial class TaskThree : UserControl {
        public TaskThree() {
            InitializeComponent();
            DataContextChanged += TaskThreeDataContextChanged;
            this.DataContext = new TaskThreeViewModel();
            HViewPort.EnableCurrentPosition = true;
            this.Loaded += TaskThreeLoaded;
        }

        private void TaskThreeDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if (sender != null) {
                var view = (TaskThree)sender;
                var viewModel = (TaskThreeViewModel)view.DataContext;
                viewModel.Viewport = HViewPort;
            }
        }

        private void TaskThreeLoaded(object sender, RoutedEventArgs e) {
            if (sender != null) {
                var view = (TaskThree)sender;
                var viewModel = (TaskThreeViewModel)view.DataContext;
                viewModel.OnButtonDrawClick(this);
            }
        }
    }
}
