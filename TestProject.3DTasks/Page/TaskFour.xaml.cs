﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TestProject.Tasks3D.ViewModel;

namespace TestProject.Tasks3D.Page {
    /// <summary>
    /// Interaction logic for TaskFour.xaml
    /// </summary>
    public partial class TaskFour : UserControl {
        public TaskFour() {
            InitializeComponent();
            DataContextChanged += TaskFourDataContextChanged;
            this.DataContext = new TaskFourViewModel();
            this.Loaded += TaskFourLoaded;
        }

        private void TaskFourDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if (sender != null) {
                var view = (TaskFour)sender;
                var viewModel = (TaskFourViewModel)view.DataContext;
                viewModel.Viewport = HViewPort;
            }
        }

        private void TaskFourLoaded(object sender, RoutedEventArgs e) {
            if (sender != null) {
                var view = (TaskFour)sender;
                var viewModel = (TaskFourViewModel)view.DataContext;
                viewModel.OnButtonDrawClick(this);
            }
        }
    }
}
