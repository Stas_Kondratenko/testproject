﻿using System.Linq;
using HelixToolkit.Wpf;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using TestProject.Core.Model;
using System.IO;
using System.Windows.Input;

namespace TestProject.Tasks3D.ViewModel {
    class TaskTwoViewModel : BaseModel {
        public HelixViewport3D Viewport {
            get { return viewport; }
            set { viewport = value; }
        }
        public ICommand ShowPoints {
            get { return showPoints; }
            set { showPoints = value; }
        }
        public ICommand ShowModel {
            get { return showModel; }
            set { showModel = value; }
        }
        public ICommand ShowLines {
            get { return showLines; }
            set { showLines = value; }
        }
        public ICommand ShowFrame {
            get { return showFrame; }
            set { showFrame = value; }
        }
        public ICommand ShowLinesSeconEdition {
            get { return showLinesSecondEdition; }
            set { showLinesSecondEdition = value; }
        }

        private ICommand showPoints;
        private ICommand showModel;
        private ICommand showLines;
        private ICommand showFrame;
        private ICommand showLinesSecondEdition;
        private HelixViewport3D viewport;
        private Model3DGroup readedModel;
        private PointsVisual3D projectionPoints;
        private LinesVisual3D lines;
        private Point3DCollection points3D;

        public TaskTwoViewModel() {
            ShowPoints = new RelayCommand(OnButtonShowPointsClick);
            ShowModel = new RelayCommand(OnButtonShowModelClick);
            ShowLines = new RelayCommand(OnButtonShowLinesClick);
            ShowFrame = new RelayCommand(OnButtonShowFrameClick);
            ShowLinesSeconEdition = new RelayCommand(OnButtonShowLinesSecondEdition);
        }

        public void Initialization() {
            Viewport.Children.Clear();
            var reader = new ObjReader();
            readedModel = reader.Read(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, "Resources/3DModel/elexis.obj"));
            projectionPoints = GetXYProjection();
            lines = GetLines();
            points3D = GetPoints();
            OnButtonShowModelClick(this);
        }

        void OnButtonShowPointsClick(object obj) {
            Viewport.Children.Clear();
            Viewport.Children.Add(projectionPoints);
        }

        void OnButtonShowModelClick(object obj) {
            Viewport.Children.Clear();
            var model = new ModelVisual3D() { Content = readedModel };
            Viewport.Children.Add(model);
            Viewport.Children.Add(new DefaultLights());
        }

        void OnButtonShowLinesClick(object obj) {
            Viewport.Children.Clear();
            Viewport.Children.Add(projectionPoints);
            Viewport.Children.Add(lines);
        }

        void OnButtonShowFrameClick(object obj) {
            Viewport.Children.Clear();
            var visual = new PointsVisual3D() { Color = Colors.Blue, Size = 3 };
            visual.Points = points3D;
            Viewport.Children.Add(visual);
        }

        void OnButtonShowLinesSecondEdition(object obj) {
            Viewport.Children.Clear();
            Viewport.Children.Add(projectionPoints);
            Viewport.Children.Add(GetSeconEditionLines());
        }

        Point3DCollection GetPoints() {
            var points = new Point3DCollection();
            foreach (var temp in readedModel.Children) {
                var mesh = ((GeometryModel3D)temp).Geometry as MeshGeometry3D;
                foreach (var point in mesh.Positions) {
                    points.Add(point);
                }
            }
            return points;
        }

        PointsVisual3D GetXYProjection() {
            var visual = new PointsVisual3D() { Color = Colors.Blue, Size = 3 };
            var points = GetPoints();
            for (int i = 0; i < points.Count; i++) {
                var point = points[i];
                point.Z = 0;
                points[i] = point;
            }
            visual.Points = points;
            return visual;
        }

        LinesVisual3D GetLines() {
            var points = GetXYMinMaxPoints(GetXYProjection().Points);
            var linesVisual = new LinesVisual3D();
            linesVisual.Children.Add(GetOneLine(points[0],points[2]));
            linesVisual.Children.Add(GetOneLine(points[1], points[3]));
            return linesVisual;
        }

        LinesVisual3D GetSeconEditionLines() {
            var points = GetXYMinMaxPoints(projectionPoints.Points);
            var linesVisual = new LinesVisual3D();
            for(int i = 0; i < 4; i++) {
                if (i == 3) {
                    linesVisual.Children.Add(GetOneLine(points[i], points[0]));
                    break;
                }
                linesVisual.Children.Add(GetOneLine(points[i], points[i+1]));
            }
            return linesVisual;
        }

        LinesVisual3D GetOneLine(Point3D p1, Point3D p2) {
            var line = new LinesVisual3D() { Color = Colors.Red, Thickness = 3 };
            line.Points.Add(p1);
            line.Points.Add(p2);
            return line;
        }

        Point3DCollection GetXYMinMaxPoints(Point3DCollection points) {
            var p1 = points.OrderBy(x => x.Y).First();
            var p2 = points.OrderBy(x => x.X).First();
            var p3 = points.OrderBy(x => x.Y).Last();
            var p4 = points.OrderBy(x => x.X).Last();
            return new Point3DCollection() { p1,p2,p3,p4};
        }
    }
}
