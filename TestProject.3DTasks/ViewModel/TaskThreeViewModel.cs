﻿using HelixToolkit.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using TestProject.Core.Model;
using TestProject.Tasks3D.Model;

namespace TestProject.Tasks3D.ViewModel {
    class TaskThreeViewModel:BaseModel {

        public HelixViewport3D Viewport {
            get { return viewport; }
            set { viewport = value; }
        }
        public ICommand Draw {
            get { return draw; }
            set { draw = value; }
        }
        public ICommand MouseClick {
            get { return mouseClick; }
            set { mouseClick = value; }
        }
        public int TopsNumber {
            get { return topsNumber; }
            set {
                topsNumber = value;
                Raise();
            }
        }
       
        private HelixViewport3D viewport;
        private ICommand draw;
        private ICommand mouseClick;
        private int topsNumber;
        private Point3DCollection points;

        public TaskThreeViewModel() {
            Draw = new RelayCommand(OnButtonDrawClick);
            MouseClick = new RelayCommand(OnViewportMouseClick);
            TopsNumber = 3;
           
        }

        public void OnButtonDrawClick(object obj) {
            Viewport.Children.Clear();
            Viewport.Children.Add(new DefaultLights());
            points = GetPoints();
            var visual = new PointsVisual3D() { Color = Colors.Blue, Size = 3,Points = points };
            Viewport.Children.Add(visual);
            Viewport.Children.Add(GetLines(points));
        }

        void OnViewportMouseClick(object obj) {
            if (Viewport.Children.Count != 1) {
                var position = (Point3D)Viewport.CursorPosition;
                var sphere = GetSphere(new Point3D(position.X, position.Y, 0));
                var geometry = new GeometryModel3D() { Geometry = sphere };
                var visualMesh = new MeshVisual3D() { Content = geometry };
                var list = new List<Point>();
                foreach(var point in points) {
                    list.Add(new Point() { X = point.X, Y = point.Y});
                }
                if (IsInPolygon(list.ToArray(), new Point(position.X, position.Y))) {
                    geometry.Material = MaterialHelper.CreateMaterial(Colors.Red); 
                } else { geometry.Material = MaterialHelper.CreateMaterial(Colors.Blue); }
                Viewport.Children.Add(visualMesh);
            }
        }

        MeshGeometry3D GetSphere(Point3D point) {
            var meshBuilder = new MeshBuilder();
            meshBuilder.AddSphere(point, 1);
            return meshBuilder.ToMesh();
        }

        public static bool IsInPolygon(Point[] poly, Point p) {
            Point p1, p2;
            bool inside = false;
            if (poly.Length < 3) {
                return inside;
            }
            var oldPoint = new Point(
                poly[poly.Length - 1].X, poly[poly.Length - 1].Y);
            for (int i = 0; i < poly.Length; i++) {
                var newPoint = new Point(poly[i].X, poly[i].Y);
                if (newPoint.X > oldPoint.X) {
                    p1 = oldPoint;
                    p2 = newPoint;
                } else {
                    p1 = newPoint;
                    p2 = oldPoint;
                }
                if ((newPoint.X < p.X) == (p.X <= oldPoint.X)
                    && (p.Y - (long)p1.Y) * (p2.X - p1.X)
                    < (p2.Y - (long)p1.Y) * (p.X - p1.X)) {
                    inside = !inside;
                }
                oldPoint = newPoint;
            }
            return inside;
        }

        LinesVisual3D GetLines(Point3DCollection points) {
            var linesVisual = new LinesVisual3D();
            for (int i = 0; i < points.Count; i++) {
                if (i == points.Count-1) {
                    linesVisual.Children.Add(GetOneLine(points[i], points[0]));
                    break;
                }
                linesVisual.Children.Add(GetOneLine(points[i], points[i + 1]));
            }
            return linesVisual;
        }

        LinesVisual3D GetOneLine(Point3D p1, Point3D p2) {
            var line = new LinesVisual3D() { Color = Colors.Green, Thickness = 3 };
            line.Points.Add(p1);
            line.Points.Add(p2);
            return line;
        }

        Point3DCollection GetPoints() {
            var random = new Random();
            var center = new System.Drawing.PointF(0, 0);
            var vertexes = TopsNumber;
            var radius = random.Next(10, 90);
            var angle = Math.PI * 2 / vertexes;
            var pointz = Enumerable.Range(0, vertexes)
                  .Select(i => System.Drawing.PointF.Add(center, new System.Drawing.SizeF((float)Math.Sin(i * angle) * radius, (float)Math.Cos(i * angle) * radius)));
            var points = new Point3DCollection();
            foreach (var point in pointz) {
                points.Add(new Point3D() { X = point.X, Y = point.Y, Z = 0 });
            }
            return points;
        }
    }
}
