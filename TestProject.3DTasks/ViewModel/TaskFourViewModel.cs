﻿using HelixToolkit.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using TestProject.Core.Model;

namespace TestProject.Tasks3D.ViewModel {
    class TaskFourViewModel:BaseModel {
        public int Scale {
            get { return scale; }
            set {
                scale = value;
                Raise();
                ScaleModel(); 
            }
        }
        private int scale;

        public HelixViewport3D Viewport {
            get { return viewport; }
            set { viewport = value; }
        }
        public ICommand Draw {
            get { return draw; }
            set { draw = value; }
        }
        public int TopsNumber {
            get { return topsNumber; }
            set {
                topsNumber = value;
                Raise();
            }
        }

        private HelixViewport3D viewport;
        private ICommand draw;
        private int topsNumber;
        private Point3DCollection points;

        public TaskFourViewModel() {
            Draw = new RelayCommand(OnButtonDrawClick);
            TopsNumber = 3;  
        }

        public void OnButtonDrawClick(object obj) {
            Viewport.Children.Clear();
            Viewport.Children.Add(new DefaultLights());
            points = GetPoints();
            var visual = new PointsVisual3D() { Color = Colors.Blue, Size = 3, Points = points };
            Viewport.Children.Add(visual);
            Viewport.Children.Add(GetLines(points));
        }

        LinesVisual3D GetLines(Point3DCollection points) {
            var linesVisual = new LinesVisual3D();
            for (int i = 0; i < points.Count; i++) {
                if (i == points.Count - 1) {
                    linesVisual.Children.Add(GetOneLine(points[i], points[0]));
                    break;
                }
                linesVisual.Children.Add(GetOneLine(points[i], points[i + 1]));
            }
            return linesVisual;
        }

        LinesVisual3D GetOneLine(Point3D p1, Point3D p2) {
            var line = new LinesVisual3D() { Color = Colors.Blue, Thickness = 3 };
            line.Points.Add(p1);
            line.Points.Add(p2);
            return line;
        }

        Point3DCollection GetPoints() {
            var random = new Random();
            var center = new System.Drawing.PointF(0, 0);
            var vertexes = TopsNumber;
            var radius = random.Next(10, 90);
            var angle = Math.PI * 2 / vertexes;

            var pointz = Enumerable.Range(0, vertexes)
                  .Select(i => System.Drawing.PointF.Add(center, new System.Drawing.SizeF((float)Math.Sin(i * angle) * radius, (float)Math.Cos(i * angle) * radius)));
            var points = new Point3DCollection();
            foreach (var point in pointz) {
                points.Add(new Point3D() { X = point.X, Y = point.Y, Z = 0 });
            }
            return points;
        }

        void ScaleModel() {
            double localScale = 0;
            var model = new MeshVisual3D();
            if (Viewport != null) {
                switch (Scale) {
                    case 0: localScale = 1; break;
                    case 1: localScale = 1.05; break;
                    case 2: localScale = 1.1; break;
                    case 3: localScale = 1.15; break;
                    case 4: localScale = 1.20; break;
                    case 5: localScale = 1.25; break;
                    case 6: localScale = 1.30; break;
                    case 7: localScale = 1.35; break;
                    case 8: localScale = 1.40; break;
                    case 9: localScale = 1.45; break;
                    case 10: localScale = 1.50; break;
                }
                foreach(var child in Viewport.Children) {
                    child.Transform = new ScaleTransform3D(localScale, localScale, 0);
                }
            }
            
        }
    }
}
