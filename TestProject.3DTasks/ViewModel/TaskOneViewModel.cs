﻿using HelixToolkit.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Media3D;
using TestProject.Core.Model;
using System.Windows.Media;
using System.Windows;
using TestProject.Tasks3D.Model;
using System.Windows.Input;

namespace TestProject.Tasks3D.ViewModel {
    class TaskOneViewModel:BaseModel {
        public Range Range {
            get { return range; }
            set {
                range = value;
                Raise();
            }
        }
        public ICommand Refresh {
            get { return refresh; }
            set {refresh = value;}
        }
        public HelixViewport3D Viewport {
            get { return viewport; }
            set { viewport = value;}
        }

        private Range range;
        private ICommand refresh;
        private HelixViewport3D viewport;

        public TaskOneViewModel() {
            Range = new Range();
            Refresh = new RelayCommand(OnButtonRefreshClick);
        }

        public void Draw() {
            Viewport.Children.Clear();
                try {
                    var points = GetPoints();
                    DrawPoints(points);
                    var sortedPoints = GetSortedPoints(points);
                    DrawLines(sortedPoints.Take(10),Colors.Green);
                    DrawLines(sortedPoints.Skip(10).Take(10), Colors.Yellow);
                    DrawLines(sortedPoints.Skip(20).Take(10), Colors.Blue);
                } catch(Exception ex) {
                    MessageBox.Show(ex.ToString());
                }
           }

        Point3DCollection AddPointsToCollection(IEnumerable<Point3D> points) {
            var localPoints = new Point3DCollection();
            foreach (var point in points) {
                localPoints.Add(new Point3D(0, 0, 0));
                localPoints.Add(point);
            }
            return localPoints;
        }

        void OnButtonRefreshClick(object obj) {
            Viewport.Children.Clear();
            Draw();
        }

        void DrawPoints(Point3DCollection points) {
            var pointsVisual = new PointsVisual3D();
            pointsVisual.Size = 5;
            pointsVisual.Points = points;
            Viewport.Children.Add(pointsVisual);
        }

        void DrawLines(IEnumerable<Point3D> points, Color color) {
            var linesVisual = new LinesVisual3D();
            foreach(var point in points) {
                var line = new LinesVisual3D();
                line.Points.Add(new Point3D(0, 0, 0));
                line.Points.Add(point);
                line.Color = color;
                linesVisual.Children.Add(line);
            }
            Viewport.Children.Add(linesVisual);
        }

        Point3DCollection GetPoints() {
            var random = new Random();
            var points = new Point3DCollection();
            for (int i = 0; i < 30; i++) {
                var point = new Point3D(
                    random.Next(Range.XMinRange, Range.XMaxRange),
                    random.Next(Range.YMinRange, Range.YMaxRange),
                    random.Next(Range.ZMinRange, Range.ZMaxRange));
                points.Add(point);
            }
            return points;
        }

        IEnumerable<Point3D> GetSortedPoints(Point3DCollection points) {
            var container = new List<PointContainer>();
            foreach (var point in points) {
                var distance = Point3DExtensions.DistanceTo(new Point3D(0, 0, 0), point);
                container.Add(new PointContainer() { Distance = distance, Point = point });
            }
            return container.OrderBy(x => x.Distance).Select(x => x.Point);
        }
    }
}
