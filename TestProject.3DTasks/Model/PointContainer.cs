﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using TestProject.Core.Model;

namespace TestProject.Tasks3D.Model {
    class PointContainer:BaseModel {
        public Point3D Point {
            get { return point; }
            set {
                point = value;
                Raise();
            }
        }
        public double Distance {
            get { return distance; }
            set {
                distance = value;
                Raise();
            }
        }

        private Point3D point;
        private double distance;
    }
}
