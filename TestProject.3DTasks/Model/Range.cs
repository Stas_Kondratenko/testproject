﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject.Core.Model;

namespace TestProject.Tasks3D.Model {
    public class Range:BaseModel {
        public int XMaxRange {
            get { return xMaxRange; }
            set {
                xMaxRange = value;
                Raise();
            }
        }
        public int XMinRange {
            get { return xMinRange; }
            set {
                xMinRange = value;
                Raise();
            }
        }

        public int YMaxRange {
            get { return yMaxRange; }
            set {
                yMaxRange = value;
                Raise();
            }
        }
        public int YMinRange {
            get { return yMinRange; }
            set {
                yMinRange = value;
                Raise();
            }
        }

        public int ZMinRange {
            get { return zMinRange; }
            set {
                zMinRange = value;
                Raise();
            }
        }
        public int ZMaxRange {
            get { return zMaxRange; }
            set {
                zMaxRange = value;
                Raise();
            }
        }

        private int xMaxRange;
        private int xMinRange;

        private int yMinRange;
        private int yMaxRange;

        private int zMaxRange;
        private int zMinRange;

        public Range() {
            XMaxRange = 20;
            XMinRange = -20;
            YMinRange = -30;
            YMaxRange = 30;
            ZMaxRange = 10;
            ZMinRange = -10;
        }
 
    }
}
