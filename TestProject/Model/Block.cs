﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject.Core.Model;

namespace TestProject.TaskWPF.Model {
    class Block:BaseModel {
        public string Status {
            get { return status; }
            set {
                status = value;
                Raise();
            }
        }
        public int MaterialBlockId {
            get { return materialBlockId; }
            set {
                materialBlockId = value;
                Raise();
            }
        }
        public int Height {
            get { return height; }
            set {
                height = value;
                Raise();
            }
        }
        public decimal Shrink {
            get { return shrink; }
            set {
                shrink = value;
                Raise();
            }
        }
        public string ColorType {
            get { return colorType; }
            set {
                colorType = value;
                Raise();
            }
        }
        public string ArticleNumber {
            get { return articleNumber; }
            set {
                articleNumber = value;
                Raise();
            }
        }
        public string ImagePath {
            get { return imagePath; }
            set {
                imagePath = value;
                Raise();
            }
        }

        private string status;
        private int materialBlockId;
        private int height;
        private decimal shrink;
        private string colorType;
        private string articleNumber;
        private string imagePath;
    }
    
}
