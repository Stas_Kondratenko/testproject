﻿using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Input;
using TestProject.Core.Model;
using TestProject.TaskWPF.Model;

namespace TestProject.TaskWPF.ViewModel {
    class MainViewModel:BaseModel {

        public ObservableCollection<Block> Blocks {
            get { return blocks;  }
            set {
                blocks = value;
                Raise();
            }
        }
        public ICommand Refresh {
            get { return refresh; }
            set {
                refresh = value;
                Raise();
            }
        }
        public ICommand Add {
            get { return add; }
            set {
                add = value;
                Raise();
            }
        }

        private ObservableCollection<Block> blocks;
        private ICommand refresh;
        private ICommand add;

        public MainViewModel() {
            Blocks = new ObservableCollection<Block>();
            Init();
            Refresh = new RelayCommand(OnButtonRefreshClick);
            Add = new RelayCommand(OnButtonAddClick);
        }

        private void Init() {
            Blocks.Add(new Block() { ArticleNumber = "TMAD0116", ColorType = "Temp Basic A1-B1", Height = 16, MaterialBlockId = 4, Shrink = 0, Status = "Tool Path Available",ImagePath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, "Resources/img/1.png") });
            Blocks.Add(new Block() { ArticleNumber = "ZRAB8011", ColorType = "Transluzent", Height = 12, MaterialBlockId = 3, Shrink = 0, Status = "Tool Path Available", ImagePath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, "Resources/img/2.png") });
            Blocks.Add(new Block() { ArticleNumber = "TMAZ0112", ColorType = "Barnout green", Height = 12, MaterialBlockId = 1, Shrink = 0, Status = "Tool Path Available", ImagePath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, "Resources/img/3.png") });
        }

        void OnButtonRefreshClick(object obj) {
            Blocks.Clear();
            Init();
        }

        void OnButtonAddClick(object obj) {
            Init();
        }
    }
}
