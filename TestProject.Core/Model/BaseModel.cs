﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.Core.Model {
    abstract public class BaseModel:INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged = (x, y) => { };

        public void Raise([CallerMemberName] string propertyName = null) {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
